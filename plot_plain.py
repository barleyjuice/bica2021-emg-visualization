import matplotlib.pyplot as plt

import numpy as np

from scipy.signal import lfilter

import os

print(f"Current working firectory: {os.getcwd()}")

data_folder = os.path.join(os.getcwd(), "data")

thumb_logs = os.path.join(data_folder, "thumb")
thumb_logs_begin = 1
thumb_logs_end = 68

rest_logs = os.path.join(data_folder, "rest")
rest_logs_begin = 1
rest_logs_end = 100

forefinger_logs = os.path.join(data_folder, "forefinger")
forefinger_logs_begin = 1
forefinger_logs_end = 23

littlefinger_logs = os.path.join(data_folder, "littlefinger")
littlefinger_logs_begin = 1
littlefinger_logs_end = 23

middlefinger_logs = os.path.join(data_folder, "middlefinger")
middlefinger_logs_begin = 1
middlefinger_logs_end = 23

ringfinger_logs = os.path.join(data_folder, "ringfinger")
ringfinger_logs_begin = 1
ringfinger_logs_end = 23

# show_first_n = 6


# plot raw data
def save_plots(first_log_index, last_log_index, basedir):
    os.makedirs(os.path.join(basedir, 'plots'), exist_ok=True)
    for i in range(first_log_index, last_log_index):
        with open(os.path.join(basedir, f"{i}.txt")) as f:
            lines = f.readlines()
            y = [int(line.replace('\n', '')) for line in lines]
            x = [i for i in range(0, len(lines))]
            plt.plot(x, y)
            plt.xlabel("ticks")
            plt.ylabel("\u03BCV")
            plt.gca().set_ylim([0, 700])
            plt.savefig(os.path.join(basedir, 'plots', f"{i}.png"))
            plt.close()

# print("Save plots for forefinger")
# save_plots(forefinger_logs_begin, forefinger_logs_end, forefinger_logs)

# print("Save plots for thumb")
# save_plots(thumb_logs_begin, thumb_logs_end, thumb_logs)

# print("Save plots for rest")
# save_plots(rest_logs_begin, rest_logs_end, rest_logs)

# print("Save plots for littlefinger")
# save_plots(littlefinger_logs_begin, littlefinger_logs_end, littlefinger_logs)

print("Save plots for middlefinger")
save_plots(middlefinger_logs_begin, middlefinger_logs_end, middlefinger_logs)

print("Save plots for ringfinger")
save_plots(ringfinger_logs_begin, ringfinger_logs_end, ringfinger_logs)

# average data between logs (showed bad results, non-informative)
def save_mean(first_log_index, last_log_index, basedir):
    os.makedirs(os.path.join(basedir, 'plots'), exist_ok=True)
    acc = []
    for i in range(first_log_index, last_log_index):
        print(i)
        with open(os.path.join(basedir, f"{i}.txt")) as f:
            lines = f.readlines()
            y = [int(line.replace('\n', '')) for line in lines]
            acc.append(y)
    
    pad = len(min(acc, key=len))
    
    for i in range(0, len(acc)):
        acc[i] = acc[i][:pad]
    
    mean = np.mean(acc, axis=1)
    x = [i for i in range(0, len(mean))]
    plt.plot(x, mean)
    plt.xlabel("ticks")
    plt.ylabel("\u03BCV")
    plt.gca().set_ylim([0, 700])
    plt.savefig(os.path.join(basedir, 'plots', f"mean.png"))
    plt.close()

# plot filtered data
def save_plots_normalized(first_log_index, last_log_index, basedir):
    os.makedirs(os.path.join(basedir, 'plots'), exist_ok=True)
    n = 15
    b = [1.0 / n] * n
    a = 1
    for i in range(first_log_index, last_log_index):
        with open(os.path.join(basedir, f"{i}.txt")) as f:
            lines = f.readlines()
            y = [int(line.replace('\n', '')) for line in lines]
            x = [i for i in range(0, len(lines))]
            plt.plot(x, lfilter(b, a, y))
            plt.xlabel("ticks")
            plt.ylabel("\u03BCV")
            plt.gca().set_ylim([0, 700])
            plt.savefig(os.path.join(basedir, 'plots', f"{i}_norm.png"))
            plt.close()

# print("Save filtered plots for forefinger")
# save_plots_normalized(forefinger_logs_begin, forefinger_logs_end, forefinger_logs)

# print("Save filtered plots for thumb")
# save_plots_normalized(thumb_logs_begin, thumb_logs_end, thumb_logs)

# print("Save filtered plots for rest")
# save_plots_normalized(rest_logs_begin, rest_logs_end, rest_logs)

# print("Save filtered plots for littlefinger")
# save_plots_normalized(littlefinger_logs_begin, littlefinger_logs_end, littlefinger_logs)

print("Save filtered plots for middlefinger")
save_plots_normalized(middlefinger_logs_begin, middlefinger_logs_end, middlefinger_logs)

print("Save filtered plots for ringfinger")
save_plots_normalized(ringfinger_logs_begin, ringfinger_logs_end, ringfinger_logs)
